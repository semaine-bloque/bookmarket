package net.tncy.det.bookmarket.data;

import net.tncy.det.validator.ISBN;

public class Book {
  private int id;
  private String title;
  private String author;
  private String publisher;
  private Bookformat format;

  @ISBN
  private String isbn; 

  public Book(int id, String title, String author, String publisher, Bookformat format, String isbn) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.publisher = publisher;
    this.format = format;
    this.isbn = isbn;
  }

  public String getTitle() {
    return title;
  }
  public String getIsbn() {
    return isbn;
  }
  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }
  public Bookformat getFormat() {
    return format;
  }
  public void setFormat(Bookformat format) {
    this.format = format;
  }
  public String getPublisher() {
    return publisher;
  }
  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public void setTitle(String title) {
    this.title = title;
  }

}
